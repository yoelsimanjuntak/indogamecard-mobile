/**
 * Created by Toshiba on 5/11/2019.
 */
var INDOGAMECARDURL = "https://dev.indogamecard.com/Ajax/";
var INDOGAMECARDURL_BASE = "https://dev.indogamecard.com/";
//var INDOGAMECARDURL = "http://localhost:54796/Ajax/";
//var INDOGAMECARDURL_BASE = "http://localhost:54796/";
var COMMON_OK = "COMMON_OK";

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function setLocalStorage(key, val) {
    localStorage.setItem(key, JSON.stringify(val));
}

function getLocalStorage(key) {
    var ls = localStorage.getItem(key);
    if(isJson(ls)) {
        return JSON.parse(ls);
    }
    else {
        return ls;
    }
}

function removeLocalStorage(key) {
    localStorage.removeItem(key);
}

function getData(action, data, successCB, failCB, doneCB) {
    var url = INDOGAMECARDURL + action;
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        crossDomain: true,
        success: successCB,
        error: failCB,
        complete: function() {
            if(doneCB) (doneCB)();
        }
    });
}

function drawFeaturedProduct(el) {
    var prods = getLocalStorage('featured');
    if(prods) {
        el.empty();
        el.closest('.list').removeClass('skeleton-text skeleton-effect-fade');

        var html = '';
        for(var i=0; i<prods.length; i++) {
            /*html += '<li>';
            html += '<a href="/voucher_detail/'+prods[i].Voucher_Id+'/" class="item-link item-content">';
            html += '<div class="item-media">';
            html += '<img src="'+prods[i].Icon_Image_File+'" width="40" height="40"/>';
            html += '</div>';
            html += '<div class="item-inner">';
            html += '<div class="item-title-row">';
            html += '<div class="item-title">'+prods[i].Voucher_Name+'</div>';
            html += '</div>';
            html += '<div class="item-subtitle">'+prods[i].Remarks+'</div>';
            html += '<div class="item-text">IDR '+numeral(prods[i].Price).format('0,0')+'</div>';
            html += '</div>';
            html += '</a>';
            html += '</li>';*/

            html += '<div class="col-50" style="/*margin: 10px 0px; */padding: 15px 0px; border: 1px solid #dedede; height: 110px;">';
            html += '<a href="/voucher_detail/'+prods[i].Voucher_Id+'/" class="link" style="padding: 0px 10px">';
            html += '<img src="'+prods[i].Icon_Image_File+'" style="width: 50px; height: 50px" />';
            html += '<p class="text-color-black no-margin" style="padding: 0px 5px">'+prods[i].Voucher_Name+'</p>';
            html += '</a>';
            /*html += '<div class="chip">';
            html += '<div class="chip-media bg-color-blue">';
            html += '<i class="icon f7-icons">add_round</i>';
            html += '</div>';
            html += '<div class="chip-label">Add Contact</div>';
            html += '</div>';*/
            html += '<p class="text-color-gray" style="font-size: 8.5pt; font-style: italic; text-align: right; margin: 0em 1em">Mulai IDR '+numeral(prods[i].Discount_Price).format('0,0')+'</p>';
            if(prods[i].Discount_Value > 0) html += '<p class="text-color-green" style="font-size: 8.5pt; font-style: italic; text-align: right; margin: 0em 1em">Disc. '+numeral(prods[i].Discount_Value).format('0,00')+'%</p>';
            html += '</div>';
        }
        el.html(html);
    }
}

function drawAllProduct(el) {
    var prods = getLocalStorage('products');
    if(prods) {
        el.empty();
        el.closest('.list').removeClass('skeleton-text skeleton-effect-fade');

        var html = '';
        for(var i=0; i<prods.length; i++) {
            if(prods[i].Voucher_Code == 'top-up') continue;

            html += '<li>';
            html += '<a href="/voucher_detail/'+prods[i].Id+'/" class="item-link item-content">';
            html += '<div class="item-media">';
            html += '<img src="'+prods[i].Icon_Image_File+'" width="40" height="40"/>';
            html += '</div>';
            html += '<div class="item-inner">';
            html += '<div class="item-title-row">';
            html += '<div class="item-title" style="white-space: normal !important;">'+prods[i].Voucher_Name+'</div>';
            html += '</div>';
            //html += '<div class="item-subtitle">'+prods[i].Remarks+'</div>';
            //html += '<div class="item-text">IDR '+numeral(prods[i].Price).format('0,0')+'</div>';
            html += '</div>';
            html += '</a>';
            html += '</li>';
        }
        el.html(html);
    }
}

function drawProductItems(data, el) {
    if(data) {
        el.empty();
        el.closest('.list').removeClass('skeleton-text skeleton-effect-fade');

        var html = '';
        var oneChecked = false;
        for(var i=0; i<data.length; i++) {
            var html = '';
            var checked = '';
            /*html += '<li>';
            html += '<div class="item-content">';
            html += '<div class="item-inner">';
            html += '<div class="item-title-row">';
            html += '<div class="item-title" style="white-space: normal !important;">'+data[i].Remarks+'</div>';
            html += '<div class="item-after">'+numeral(data[i].Nominal).format('0,0')+'</div>';
            html += '</div>';
            if(data[i].Discount_Id) {
                html += '<div class="item-subtitle">'+data[i].Discount_Description+'</div>';
            }
            html += '<div class="item-text text-align-center" style="display: block !important;">';
            html += '<div class="row" style="margin-top: 5px">';
            html += '<div class="col">';

            var stepper = '';
            stepper += '<div class="stepper stepper-small stepper-raised stepper-fill stepper-el-'+data[i].Id+'">';
            stepper += '<div class="stepper-button-minus"></div>';
            stepper += '<div class="stepper-input-wrap"><input type="text" value="0" min="0" max="100" step="1" readonly></div>';
            stepper += '<div class="stepper-button-plus"></div>';
            stepper += '</div>';
            html += stepper;
            html += '</div>';
            html += '</div>';
            html += '</div>';

            html += '</div>';
            html += '</div>';
            html += '</li>';*/

            if(data[i].Discount_Price == 0) {
                continue;
            }

            if((i==0 || !oneChecked) && (data[i].Voucher_Code == 'top-up' || data[i].Available_Stock > 0)) {
                checked = 'checked';
                oneChecked = true;
            }
            html += '<li>';
            html += '<label class="item-radio item-content">';
            html += '<input type="radio" name="Voucher_Detail_Id" value="'+data[i].Id+'" '+checked+(data[i].Voucher_Code == 'top-up' || data[i].Available_Stock > 0?'':'disabled')+' />';
            html += '<input type="hidden" name="Discount_Price" value="'+(data[i].Voucher_Code == 'top-up' || data[i].Available_Stock > 0 ? data[i].Discount_Price : 0)+'" />';
            html += '<i class="icon icon-radio" '+(i==0?'checked':'')+'></i>';
            html += '<div class="item-inner">';
            html += '<div class="item-title-row">';
            html += '<div class="item-title" style="white-space: normal !important;">'+data[i].Remarks+'</div>';
            html += '</div>';
            if(data[i].Voucher_Code == 'top-up' || data[i].Available_Stock > 0) {
                html += '<div class="item-subtitle">IDR '+(data[i].Price != data[i].Discount_Price ? '<span class="text-color-gray" style="text-decoration: line-through">'+numeral(data[i].Price).format('0,0')+'</span> '+ numeral(data[i].Discount_Price).format('0,0') : numeral(data[i].Discount_Price).format('0,0'))+'</div>';
            } else {
                html += '<div class="item-subtitle"><span class="badge color-red">Out of stock</span></div>';
            }

            html += '</div>';
            html += '</label>';
            html += '</li>';

            el.append(html);
        }
        //el.html(html);
    }
}

function drawCarts(data, el) {
    var auth = getLocalStorage('auth');
    if(data) {
        el.empty();
        el.closest('.list').removeClass('skeleton-text skeleton-effect-fade');

        var html = '';
        for(var i=0; i<data.length; i++) {
            var html = '';

            html += '<li class="swipeout">';
            html += '<div class="item-content swipeout-content">';
            html += '<div class="item-inner">';
            html += '<div class="item-title">'+data[i].voucher_item+'</div>';
            html += '<div class="item-after"><span class="badge">'+data[i].voucher_qty+'</span></div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="swipeout-actions-right">';
            html += '<a href="#" data-confirm="Anda yakin ingin menghapus??" data-id="'+data[i].cart_id+'" class="swipeout-delete">Hapus</a>';
            html += '</div>';
            html += '</li>';

            /*$$(html).on('swipeout:delete', function(e) {
                var id = $$(this).find('.swipeout-delete').data('id');
                alert('delete '+id);
            });*/

            el.append(html);
        }

        $$('.swipeout').on('swipeout:delete', function () {
            var id = $(this).find('.swipeout-delete').data('id');

            app.preloader.show();
            getData('RemoveCart', {id: id, UUID: auth.UUID}, function(data) {
                var dat = data.res;
                if(dat.Status == COMMON_OK) {
                    var toast = app.toast.create({
                        text: 'Item dihapus.',
                        icon: '<i class="f7-icons">trash</i>',
                        position: 'center',
                        closeTimeout: 2000
                    });
                    toast.open();
                }
                else {
                    app.dialog.alert(dat.Message);
                }
            }, function() {
                app.dialog.alert("Something gone wrong..");
            }, function() {
                app.preloader.hide();
            });
        });
    }
}

function drawBankLists(data, el) {
    var auth = getLocalStorage('auth');
    el.empty();
    el.closest('.list').removeClass('skeleton-text skeleton-effect-fade');
    if(data) {
        var html = '';
        for(var i=0; i<data.length; i++) {
            var html = '';
            var desc = data[i].Bank_Description;
            if(desc.toLowerCase().includes("auto")) {
                desc = 'Konfirmasi : AUTO';
            } else if(desc.toLowerCase().includes("manual")) {
                desc = 'Konfirmasi : MANUAL';
            } else {
                desc = '-';
            }

            html += '<li>';
            html += '<label class="item-radio item-content">';
            html += '<input type="radio" name="Bank_Id" value="'+data[i].Id+'" '+(i==0?'checked':'')+' />';
            html += '<i class="icon icon-radio" style="margin-right: calc(var(--f7-list-item-media-margin)) !important" '+(i==0?'checked':'')+'></i>';
            html += '<div class="item-media">';
            html += '<img src="'+data[i].Bank_Icon+'" style="width: 56px; height: 24px" />';
            html += '</div>';
            html += '<div class="item-inner">';
            html += '<div class="item-title-row">';
            html += '<div class="item-title">'+data[i].Bank_Name+'</div>';
            html += '</div>';
            html += '<div class="item-subtitle">'+data[i].Bank_Account+' A/N '+data[i].Bank_Account_Name+'</div>';
            html += '<div class="item-text">'+desc+'</div>';
            html += '</div>';
            html += '</label>';
            html += '</li>';

            el.append(html);
        }
    }
}

function drawVALists(data, el, total=0) {
    var auth = getLocalStorage('auth');
    el.empty();
    el.closest('.list').removeClass('skeleton-text skeleton-effect-fade');
    if(data) {
        var html = '';
        for(var i=0; i<data.length; i++) {
            var html = '';
            var fee = data[i].Admin_Fee;
            if(data[i].Payment_Type == "OVO") {
                fee = total * (fee/100);
            }

            html += '<li data-fee="'+fee+'">';
            html += '<label class="item-radio item-content">';
            html += '<input type="radio" name="Bank_Id" value="'+data[i].Id+'" />';
            html += '<i class="icon icon-radio" style="margin-right: calc(var(--f7-list-item-media-margin)) !important" '+(i==0?'checked':'')+'></i>';
            html += '<div class="item-media">';
            html += '<img src="'+data[i].Bank_Icon+'" style="width: 56px; height: 24px" />';
            html += '</div>';
            html += '<div class="item-inner">';
            html += '<div class="item-title-row">';
            html += '<div class="item-title">'+data[i].Bank_Name.replace("Virtual Account", "")+'</div>';
            html += '</div>';
            html += '<div class="item-subtitle">Biaya Admin: '+numeral(fee).format('0,0')+'</div>';
            html += '<div class="item-text">'+data[i].Bank_Description+'</div>';
            html += '</div>';
            html += '</label>';
            html += '</li>';

            el.append(html);
        }
    }
}


function drawIGCList(el) {
    el.empty();
    el.closest('.list').removeClass('skeleton-text skeleton-effect-fade');

    var html = '';

    html += '<li data-fee=0>';
    html += '<label class="item-radio item-content">';
    html += '<input type="radio" name="Bank_Id" value=-1 checked/>';
    html += '<i class="icon icon-radio" style="margin-right: calc(var(--f7-list-item-media-margin)) !important" checked></i>';
    html += '<div class="item-media">';
    // html += '<img src="'+data[i].Bank_Icon+'" style="width: 56px; height: 24px" />';
    html += '</div>';
    html += '<div class="item-inner">';
    html += '<div class="item-title-row">';
    html += '<div class="item-title">Full IGC Balance</div>';
    html += '</div>';
    //html += '<div class="item-subtitle">'+data[i].Bank_Account+'</div>';
    html += '<div class="item-text">Biaya Admin: '+numeral(0).format('0,0')+'</div>';
    html += '</div>';
    html += '</label>';
    html += '</li>';

    el.append(html);
}

function drawOrderHistory(el) {
    var prods = getLocalStorage('history');
    if(prods) {
        el.empty();
        el.closest('.list').removeClass('skeleton-text skeleton-effect-fade');

        var html = '';
        for(var i=0; i<prods.length; i++) {
            var items = prods[i].Items.map(function(elem){
                return elem.Description+' x '+numeral(elem.Quantity).format('0,0');
            }).join(",");

            var icon = '';
            var paymentType = prods[i].Payment_Type;
            var status = prods[i].Status;
            var amount = prods[i].Amount;
            var link = '<a href="/finish/'+prods[i].Order_Id+'/-1/" class="item-link item-content">';
            if (status == 'Not Paid') {
                icon = '<i class="icon f7-icons" style="font-size: 11pt; font-weight: bold; line-height: 1.5">stopwatch</i>';
                link = '<a href="/payment/'+prods[i].Order_Id+'/" class="item-link item-content">';
                amount = prods[i].Order_Amount;
            }
            else if (status == 'Verified') {
                icon = '<i class="icon f7-icons text-color-green" style="font-size: 11pt; font-weight: bold; line-height: 1.5">check_round</i>';
            }
            else if (status == 'Not Verified') {
                icon = '<i class="icon f7-icons" style="font-size: 11pt; font-weight: bold; line-height: 1.5">stopwatch</i>';
            }
            else if (status == 'Expired') {
                icon = '<i class="icon f7-icons text-color-red" style="font-size: 11pt; font-weight: bold; line-height: 1.5">stopwatch</i>';
            }
            else if (status == 'Waiting') {
                icon = '<i class="icon f7-icons" style="font-size: 11pt; font-weight: bold; line-height: 1.5">help_round</i>';
            }

            html += '<li>';
            html += link;
            html += '<div class="item-inner">';
            html += '<div class="item-title-row">';
            html += '<div class="item-title" style="font-weight: bold">' + prods[i].Order_Number + '</div>';
            html += '<div class="item-after">' + moment(prods[i].Order_Date).format('DD/MM/YYYY') + '&nbsp;&nbsp;' + icon + '</div>';
            html += '</div>';
            html += '<div class="item-subtitle">' + items + '</div>';
            html += '<div class="item-text">'+ paymentType + " Rp " + numeral(amount).format('0,0') + '</div>';
            html += '</div>';
            html += '</a>';
            html += '</li>';
        }
        el.html(html);
    }
}

function drawPointHistory(el) {
    var prods = getLocalStorage('points');
    if(prods) {
        el.empty();
        el.closest('.list').removeClass('skeleton-text skeleton-effect-fade');

        var html = '';
        for(var i=0; i<prods.length; i++) {

            var point = numeral(prods[i].Amount).format('0,0');
            if(!prods[i].Is_Credit) {
                point = '<span class="text-color-green">+'+numeral(prods[i].Amount).format('0,0')+'</span>';
            }
            else {
                point = '<span class="text-color-red">-'+numeral(prods[i].Amount).format('0,0')+'</span>';
            }
            html += '<li>';
            html += '<a href="#" class="item-link item-content">';
            html += '<div class="item-inner">';
            html += '<div class="item-title-row">';
            html += '<div class="item-title">'+moment(prods[i].Create_Date).format('DD/MM/YYYY')+'</div>';
            html += '<div class="item-after" style="font-weight: bold">'+point+'</div>';
            html += '</div>';
            html += '<div class="item-subtitle">'+prods[i].Description+'</div>';
            html += '</div>';
            html += '</a>';
            html += '</li>';
        }
        el.html(html);
    }
}

function drawNews(data, el) {
    if(data) {
        el.empty();
        el.closest('.list').removeClass('skeleton-text skeleton-effect-fade');

        var html = '';
        for(var i=0; i<data.length; i++) {
            html += '<li>';
            html += '<a href="/news_detail/'+data[i].Id+'/" class="item-link item-content">';
            html += '<div class="item-inner">';
            html += '<div class="item-title-row">';
            html += '<div class="item-title">'+data[i].News_Name+'</div>';
            //html += '<div class="item-after">'+moment(data[i].News_Date).format('DD/MM/YYYY')+'</div>';
            html += '</div>';

            //html += '<div class="item-subtitle">'+moment(data[i].News_Date).format('DD/MM/YYYY')+'</div>';
            html += '<div class="item-text">'+data[i].News_Description+'</div>';
            html += '</div>';
            html += '</a>';
            html += '</li>';
        }
        el.html(html);
    }
}