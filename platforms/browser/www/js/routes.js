routes = [
    {
        path: '/',
        url: './index.html'
    },
    {
        path: '/about/',
        url: './pages/about.html'
    },
    {
        path: '/catalog/',
        componentUrl: './pages/catalog.html'
    },
    {
        path: '/product/:id/',
        componentUrl: './pages/product.html'
    },
    {
        path: '/voucher/',
        url: './pages/voucher.html'
    },
    {
        path: '/news/',
        url: './pages/news.html'
    },
    {
        path: '/voucher_detail/:id/',
        componentUrl: './pages/voucher_detail.html'
    },
    {
        path: '/news_detail/:id/',
        componentUrl: './pages/news_detail.html'
    },
    {
        path: '/settings/',
        url: './pages/settings.html'
    },
    {
        path: '/profile/',
        url: './pages/profile.html'
    },
    {
        path: '/settings/',
        url: './pages/settings.html'
    },
    {
        path: '/cart/',
        url: './pages/cart.html'
    },
    {
        path: '/history/',
        url: './pages/history.html'
    },
    {
        path: '/historypoint/',
        url: './pages/historypoint.html'
    },
    {
        path: '/topup/',
        url: './pages/topup.html'
    },
    {
        path: '/payment/:orderId/',
        componentUrl: './pages/payment.html'
    },
    {
        path: '/finish/:paymentId/:mode/',
        componentUrl: './pages/finish.html'
    },
    // Page Loaders & Router
    {
        path: '/page-loader-template7/:user/:userId/:posts/:postId/',
        templateUrl: './pages/page-loader-template7.html'
    },
    {
        path: '/page-loader-component/:user/:userId/:posts/:postId/',
        componentUrl: './pages/page-loader-component.html'
    },
    {
        path: '/howto/',
        url: './pages/_howto.html'
    },
    {
        path: '/policy/',
        url: './pages/_policy.html'
    },
    {
        path: '/terms/',
        url: './pages/_terms.html'
    },
    {
        path: '/request-and-load/user/:userId/',
        async: function (routeTo, routeFrom, resolve, reject) {
            // Router instance
            var router = this;

            // App instance
            var app = router.app;

            // Show Preloader
            app.preloader.show();

            // User ID from request
            var userId = routeTo.params.userId;

            // Simulate Ajax Request
            setTimeout(function () {
                // We got user data from request
                var user = {
                    firstName: 'Vladimir',
                    lastName: 'Kharlampidi',
                    about: 'Hello, i am creator of Framework7! Hope you like it!',
                    links: [
                        {
                            title: 'Framework7 Website',
                            url: 'http://framework7.io'
                        },
                        {
                            title: 'Framework7 Forum',
                            url: 'http://forum.framework7.io'
                        },
                    ]
                };
                // Hide Preloader
                app.preloader.hide();

                // Resolve route to load page
                resolve(
                    {
                        componentUrl: './pages/request-and-load.html'
                    },
                    {
                        context: {
                            user: user
                        }
                    }
                );
            }, 1000);
        }
    },
    // Default route (404 page). MUST BE THE LAST
    {
        path: '(.*)',
        url: './pages/404.html'
    },
];

//routes = [
//  {
//    path: '/',
//    url: './index.html'
//  },
//  {
//    path: '/about/',
//    url: './pages/about.html'
//  },
//  {
//    path: '/catalog/',
//    componentUrl: './pages/catalog.html'
//  },
//  {
//    path: '/product/:id/',
//    componentUrl: './pages/product.html'
//  },
//  {
//    path: '/settings/',
//    url: './pages/settings.html'
//  },
//
//    {
//        path: '/cart/',
//        componentUrl: './pages/cart.html'
//    },
//  // Page Loaders & Router
//  {
//    path: '/page-loader-template7/:user/:userId/:posts/:postId/',
//    templateUrl: './pages/page-loader-template7.html'
//  },
//  {
//    path: '/page-loader-component/:user/:userId/:posts/:postId/',
//    componentUrl: './pages/page-loader-component.html'
//  },
//  {
//    path: '/request-and-load/user/:userId/',
//    async: function (routeTo, routeFrom, resolve, reject) {
//      // Router instance
//      var router = this;
//
//      // App instance
//      var app = router.app;
//
//      // Show Preloader
//      app.preloader.show();
//
//      // User ID from request
//      var userId = routeTo.params.userId;
//
//      // Simulate Ajax Request
//      setTimeout(function () {
//        // We got user data from request
//        var user = {
//          firstName: 'Vladimir',
//          lastName: 'Kharlampidi',
//          about: 'Hello, i am creator of Framework7! Hope you like it!',
//          links: [
//            {
//              title: 'Framework7 Website',
//              url: 'http://framework7.io'
//            },
//            {
//              title: 'Framework7 Forum',
//              url: 'http://forum.framework7.io'
//            },
//          ]
//        };
//        // Hide Preloader
//        app.preloader.hide();
//
//        // Resolve route to load page
//        resolve(
//          {
//            componentUrl: './pages/request-and-load.html'
//          },
//          {
//            context: {
//              user: user,
//            }
//          }
//        );
//      }, 1000);
//    }
//  },
//  // Default route (404 page). MUST BE THE LAST
//  {
//    path: '(.*)',
//    url: './pages/404.html'
//  },
//];
