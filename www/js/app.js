document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
    document.addEventListener("backbutton", function (e) {
        e.preventDefault();
    }, false );
}

// Dom7
var $$ = Dom7;

// Framework7 App main instance
var app  = new Framework7({
  root: '#app', // App root element
  id: 'io.indogamecard.app', // App bundle ID
  name: 'Indogamecard', // App name
  theme: 'auto', // Automatic theme detection
  // App root data
  data: function () {
    return {
        carts: [
            {
                id: 1,
                title: 'Steam Wallet IDR - 250,000',
                qty: 1,
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi tempora similique reiciendis, error nesciunt vero, blanditiis pariatur dolor, minima sed sapiente rerum, dolorem corrupti hic modi praesentium unde saepe perspiciatis.'
            },
            {
                id: 2,
                title: 'Steam Wallet IDR - 60,000',
                qty: 3,
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi tempora similique reiciendis, error nesciunt vero, blanditiis pariatur dolor, minima sed sapiente rerum, dolorem corrupti hic modi praesentium unde saepe perspiciatis.'
            },
            {
                id: 1,
                title: 'Gemscool Cash ID - 30,000',
                qty: 3,
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi tempora similique reiciendis, error nesciunt vero, blanditiis pariatur dolor, minima sed sapiente rerum, dolorem corrupti hic modi praesentium unde saepe perspiciatis.'
            }
        ]
    };
  },
  // App root methods
  methods: {
      login: function() {
          var auth = getLocalStorage('auth');
          if(!auth) {
              loginScreen.open(true);
          } else {
              //alert("Logged in");
              app.methods.init();
          }
      },
      logout: function() {
          app.dialog.confirm("Anda yakin ingin keluar?", "Logout", function() {
              app.preloader.show();
              removeLocalStorage('auth');
              location.reload();
              app.preloader.hide();
          }, function() {

          });
      },
      getFeaturedProducts: function() {
          getData('GetFeaturedProduct', {count: -1}, function(data) {
              var dat = data.res;
              if(dat.Status == COMMON_OK) {
                  setLocalStorage('featured', dat.Data);
                  drawFeaturedProduct($('.featured-products-icon-only', $('.page[data-name="home"]')));
              }
              else {

              }
          }, function() {

          }, function() {

          });
      },
      getAllProducts: function() {
          getData('GetAllProduct', {}, function(data) {
              var dat = data.res;
              if(dat.Status == COMMON_OK) {
                  setLocalStorage('products', dat.Data);
                  drawAllProduct($('.all-voucher', $('.page[data-name="voucher"]')));
              }
              else {

              }
          }, function() {

          }, function() {

          });
      },
      init: function() {
          var auth = getLocalStorage('auth');
          if(auth) {
              $$('.text-name').html((auth.First_Name || '') + ' ' + (auth.Last_Name || ''));
              $$('.text-email').html((auth.Email || ''));

              app.form.fillFromData($('#profile-form', $('.page[data-name="profile"]')), auth);
          }

          setInterval(function(){
              var auth = getLocalStorage('auth');
              if(auth) {
                  getData('CheckLogin', {username: auth.Email, password: auth.password, uuid: auth.UUID}, function(data) {
                      var dat = data.res;
                      if(dat.Status == COMMON_OK) {
                          if(JSON.stringify(auth) != JSON.stringify(dat.Data)) {
                              setLocalStorage('auth', dat.Data);
                              $('.text-name', $('.page[data-name="home"]')).html((dat.Data.First_Name || '') + ' ' + (dat.Data.Last_Name || ''));
                              $('.text-email', $('.page[data-name="home"]')).html((dat.Data.Email || ''));

                              app.form.fillFromData($('#profile-form', $('.page[data-name="profile"]')), dat.Data);
                          }
                      }
                      else {
                          removeLocalStorage('auth');
                          location.reload();
                      }
                  }, function() {

                  }, function() {

                  });
              }
          }, 3000);

          setInterval(function(){
              var auth = getLocalStorage('auth');
              if(auth) {
                  getData('GetMemberPoint', {uuid: (device.platform == "browser" ? device.platform : device.uuid)}, function(data) {
                      var dat = data.res;
                      if(dat.Status == COMMON_OK) {
                          setLocalStorage('points', dat.Data);
                      }
                      else {

                      }
                  }, function() {

                  }, function() {

                  });
              }
          }, 3000);

          setInterval(function(){
              var points = getLocalStorage('points');
              if(points) {
                  var sum = 0;
                  for(var i=0; i<points.length; i++) {
                      if(!points[i].Is_Credit) sum += parseFloat(points[i].Amount);
                      else sum -= parseFloat(points[i].Amount);
                  }
                  $$(".text-point").html(numeral(sum).format('0,0.00'));
              }
          }, 3000);

          setInterval(function(){
              var auth = getLocalStorage('auth');
              if(auth) {
                  getData('GetCartCount', {UUID: auth.UUID}, function(data) {
                      $('.label-cart', $('.view#view-home')).html(numeral(data.count).format('0,0'));
                  }, function() {

                  }, function() {

                  });
              }
          }, 3000);

          app.methods.getFeaturedProducts();
          app.methods.getAllProducts();
          setInterval(function(){
              app.methods.getFeaturedProducts();
              app.methods.getAllProducts();
          }, 120000);
      }
  },
  // App routes
  routes: routes
});

// Init/Create views
var homeView = app.views.create('#view-home', {
  url: '/'
});
var cartView = app.views.create('#view-catalog', {
    url: '/news/'
});
var profileView = app.views.create('#view-profile', {
  url: '/settings/'
});

var loginScreen = app.loginScreen.create({
  el:"#my-login-screen",
  on: {
    opened: function () {
        $$('#my-login-screen .login-button').on('click', function () {
            var username_ = $$('#my-login-screen [name="username"]').val();
            var password_ = $$('#my-login-screen [name="password"]').val();

            app.preloader.show();
            getData('Login', {username: username_, password: password_, uuid: (device.platform == "browser" ? device.platform : device.uuid)}, function(data) {
                var dat = data.res;
                if(dat.Status == COMMON_OK) {
                    loginScreen.close(true);
                    setLocalStorage('auth', dat.Data);
                    app.methods.init();
                }
                else {
                    app.dialog.alert(dat.Message);
                }
            }, function() {
                app.dialog.alert("Something gone wrong..");
            }, function() {
                app.preloader.hide();
            });
        });
        $$('#my-login-screen .link-register').on('click', function () {
            registerScreen.open(true);
        });
    }
  }
});

var registerScreen = app.loginScreen.create({
    el:"#my-register-screen",
    on: {
        opened: function () {
            $$('#my-register-screen .login-button').on('click', function () {
                registerScreen.close(true);
                loginScreen.open(true);
            });
            $$('#my-register-screen .register-button').on('click', function () {
                var model = {
                    First_Name: $$('#my-register-screen [name="First_Name"]').val(),
                    Email: $$('#my-register-screen [name="Email"]').val(),
                    Password: $$('#my-register-screen [name="Password"]').val(),
                    ConfirmPassword: $$('#my-register-screen [name="ConfirmPassword"]').val(),
                    SecurityQuestionAnswer: $$('#my-register-screen [name="SecurityQuestionAnswer"]').val(),
                    ConfirmSecurityQuestionAnswer: $$('#my-register-screen [name="ConfirmSecurityQuestionAnswer"]').val()
                };

                var emptyVal = $("input", $('#my-register-screen')).not('.input-with-value');
                if(emptyVal.length > 0) {
                    app.dialog.alert("Mohon periksa data isian kembali.");
                    return;
                }

                app.preloader.show();
                getData('Register', model, function(data) {
                    var dat = data.res;
                    if(dat.Status == COMMON_OK) {
                        app.dialog.alert(dat.Message);
                        registerScreen.close(true);
                    }
                    else {
                        app.dialog.alert(dat.Message);
                    }
                }, function() {
                    app.dialog.alert("Something gone wrong..");
                }, function() {
                    app.preloader.hide();
                });
            });
        }
    }
});

var progress = 0;
var container = $$('#splashtext p:last-child');
app.progressbar.show(container, 0, 'yellow');
function simulateLoading() {
    setTimeout(function () {
        var progressBefore = progress;
        progress += 100;
        app.progressbar.set(container, progress);
        if (progressBefore < 100) {
            simulateLoading();
        }
        else {
            app.progressbar.hide(container);
            $("#splashscreen").fadeOut("slow");

            app.methods.login();
        }
    }, Math.random() * 200 + 200);
}
simulateLoading();
$$(document).on('page:init', '.page[data-name="home"]', function (e, page) {
    app.methods.init();
});

$$(document).on('page:init', '.page[data-name="voucher"]', function (e, page) {
    app.methods.getAllProducts();
});

$$(document).on('page:init', '.page[data-name="news"]', function (e, page) {
    getData('GetNews', {}, function(data) {
        setLocalStorage('news', data.res);
        drawNews(data.res, $('.news-list', page.el));
    }, function() {

    }, function() {

    });
});

$$(document).on('page:init', '.page[data-name="voucher-detail"]', function (e, page) {
    var auth = getLocalStorage('auth');

    getData('GetVoucherDetails', {id: page.route.params.id}, function(data) {
        var dat = data.res;
        if(dat.Status == COMMON_OK) {
            drawProductItems(dat.Data, $('.voucher-items', page.el));
            var stepperObj = app.stepper.create({el: '.stepper' });

            $('[name=Voucher_Detail_Id], [name=Qty]', page.el).change(function() {
                var selItem = $('[name=Voucher_Detail_Id]:checked', page.el).closest('li');
                var selPrice = $('[name=Discount_Price]', selItem).val();
                var qty = $('[name=Qty]', page.el).val();

                $('[name=Total_Amount]', page.el).val(numeral(selPrice*qty).format('0,0'))
            }).change();
        }
        else {

        }
    }, function() {

    }, function() {

    });

    $('#btn-submit, #btn-cart', page.el).click(function() {
        var selItem = $('[name=Voucher_Detail_Id]:checked', page.el).val();
        var qty = $('[name=Qty]', page.el).val();
        var id = $(this).attr("id");

        if(selItem != null && qty > 0) {
            var cart = {
                Order_Id: null,
                Voucher_Detail_id: selItem,
                Price: 0,
                Nominal: 0,
                Quantity: qty,
                Discount_Id: 0,
                Discount_Value: 0,
                Discount_Price: 0,
                Total_Amount: 0
            };

            app.preloader.show();
            getData('AddToCart', { carts: [cart], UUID: auth.UUID }, function(data) {
                var dat = data.res;
                if(dat.Status == COMMON_OK) {
                    if (id == 'btn-submit') app.views.current.router.navigate('/cart/');
                    else {
                        var toast = app.toast.create({
                            text: 'Berhasil menambah keranjang.',
                            icon: '<i class="f7-icons">check</i>',
                            position: 'center',
                            closeTimeout: 2000,
                            on: {
                                closed: function () {
                                    app.views.current.router.navigate('/');
                                }
                            }
                        });
                        toast.open();
                    }
                }
                else {
                    app.dialog.alert(dat.Message);
                }
            }, function() {
                app.dialog.alert("Something gone wrong..");
            }, function() {
                app.preloader.hide();
            });
        } else {
            var toastError = app.toast.create({
                text: 'Silakan pilih item!',
                icon: '<i class="f7-icons">close_round</i>',
                position: 'center',
                closeTimeout: 2000
            });
            toastError.open();
        }
    });
});

$$(document).on('page:init', '.page[data-name="settings"]', function (e, page) {
    var auth = getLocalStorage('auth');
    if(auth) {
        $$('.text-name').html((auth.First_Name || '') + ' ' + (auth.Last_Name || ''));
        $$('.text-email').html((auth.Email || ''));
    }
});

$$(document).on('page:init', '.page[data-name="profile"]', function (e, page) {
    var auth = getLocalStorage('auth');
    if(auth) {
        app.form.fillFromData($('#profile-form', $('.page[data-name="profile"]')), auth);
    }

    $$('.btn-submit', page.el).on('click', function() {
        var auth = getLocalStorage('auth');
        var formData = app.form.convertToData('#profile-form');
        formData = $.extend(formData, {UUID: auth.UUID});

        app.preloader.show();
        getData('UpdateProfile', formData, function(data) {
            var dat = data.res;
            if(dat.Status == COMMON_OK) {
                setLocalStorage('auth', dat.Data);
                var toast = app.toast.create({
                    text: 'Profil berhasil diupdate.',
                    icon: '<i class="f7-icons">check</i>',
                    position: 'center',
                    closeTimeout: 2000
                });
                toast.open();
            }
            else {
                app.dialog.alert(dat.Message);
            }
        }, function() {
            app.dialog.alert("Something gone wrong..");
        }, function() {
            app.preloader.hide();
            app.views.current.router.navigate('/settings/',  {reloadCurrent: true, reloadAll: true, animate: true});
        });
    });
});

$$(document).on('page:init', '.page[data-name="cart"]', function (e, page) {
    var auth = getLocalStorage('auth');

    getData('GetCart', {UUID: auth.UUID}, function(data) {
        var dat = data.carts;
        drawCarts(dat, $('.cart-items', page.el));
    }, function() {

    }, function() {

    });

    $('.btn-submit', page.el).click(function() {
        var carts = $('li.swipeout');
        if(carts.length == 0) {
            var toastError = app.toast.create({
                text: 'Keranjang kosong!',
                icon: '<i class="f7-icons">close_round</i>',
                position: 'center',
                closeTimeout: 2000
            });
            toastError.open();
        }
        else {
            var ids = [];
            for(var i=0; i<carts.length; i++) {
                ids.push($('.swipeout-delete', $(carts[i])).data('id'));
            }

            app.preloader.show();
            getData('CreateOrder', {CartIDs: ids, UUID: auth.UUID}, function(data) {
                var dat = data.res;
                if(dat.Status == COMMON_OK) {
                    var orderId = dat.Data.Id;
                    app.views.current.router.navigate('/payment/'+orderId+'/');
                }
                else {
                    app.dialog.alert(dat.Message);
                }
            }, function() {
                app.dialog.alert("Something gone wrong..");
            }, function() {
                app.preloader.hide();
            });
        }
    });
});

$$(document).on('page:init', '.page[data-name="payment"]', function (e, page) {
    var auth = getLocalStorage('auth');
    var orderId = page.route.params.orderId;

    var fullPayment = false;
    var totalCurrentPoints = 0;
    getData('GetOrderDetail', {id: parseInt(orderId), UUID: auth.UUID}, function(data) {
        var dat = data.res;
        if(dat.Status == COMMON_OK) {
            var order = dat.Data.order;
            var payment = dat.Data.payment;
            var memberPointAmount = 0;
            fullPayment = dat.Data.fullPayment;
            totalCurrentPoints = dat.Data.totalCurrentPoints;
            $('.list', page.el).removeClass('skeleton-text skeleton-effect-fade');
            $('[name=Order_id]', page.el).val(payment.Order_id);
            $('[name=Order_Date]', page.el).val(order.Order_Date);
            $('[name=Order_Number]', page.el).val(order.Order_Number);
            if (fullPayment){
                if (totalCurrentPoints > order.Amount){
                    memberPointAmount = order.Amount;
                    order.Unique_Amount = 0;
                }
                else
                    memberPointAmount = totalCurrentPoints;
            }
            $('[name=Amount]', page.el).val(order.Amount);
            $('[name=Unique_Amount]', page.el).val(order.Unique_Amount);
            $('[name=Member_Point_Amount]', page.el).val(memberPointAmount);
            $('.label-orderno', page.el).html(order.Order_Number);
            $('.label-orderdate', page.el).html(moment(order.Order_Date).format('DD/MM/YYYY'));
            $('.label-orderamt', page.el).html(numeral(order.Amount).format('0,0'));
            $('.label-orderuniq', page.el).html(numeral(order.Unique_Amount).format('0,0'));
            $('.label-memberamt', page.el).html(numeral(memberPointAmount).format('0,0'));
            $('.label-ordertotal', page.el).html(numeral(order.Amount + order.Unique_Amount - memberPointAmount).format('0,0'));

            getData('GetBankList', {}, function(data) {
                var dat = data.banks;
                var list_bank = dat.filter(x => x.Payment_Type == "Bank Transfer");
                var list_va = dat.filter(x => x.Payment_Type == "Virtual Account");
                var list_minimarket = dat.filter(x => x.Payment_Type == "Minimarket");
                var list_ovo = dat.filter(x => x.Payment_Type == "OVO");
                console.log(list_bank, "banks");
                if (fullPayment){
                    drawIGCList($('.bank-options', page.el));
                    drawVALists(null, $('.virtual-acc-options', page.el));
                    drawVALists(null, $('.minimarket-options', page.el));
                    drawVALists(null, $('.ovo-options', page.el), order.Amount);
                }
                else{
                    drawBankLists(list_bank, $('.bank-options', page.el));
                    drawVALists(list_va, $('.virtual-acc-options', page.el));
                    drawVALists(list_minimarket, $('.minimarket-options', page.el));
                    drawVALists(list_ovo, $('.ovo-options', page.el), order.Amount);
                }
        
                $("[name=Bank_Id]", page.el).change(function() {
                    var dis = $(this);
                    var li = dis.closest("li");
                    var fee = li.data("fee");
                    var amt = $('[name=Amount]', page.el).val();
                    var uniq = $('[name=Unique_Amount]', page.el).val();
                    var memberPointAmount = $('[name=Member_Point_Amount]', page.el).val();
        
                    if(fee && fee > 0) {
                        $(".payment-fee", page.el).attr("disabled", false).show();
                        $(".payment-uniq", page.el).attr("disabled", true).hide();
        
                        $('.label-orderfee', page.el).html(numeral(fee).format('0,0'));
                        $('[name=Admin_Fee]', page.el).val(fee);
                        $('.label-ordertotal', page.el).html(numeral(parseFloat(amt) - parseFloat(memberPointAmount) +parseFloat(fee)).format('0,0'));
                    } else {
                        $(".payment-fee", page.el).attr("disabled", false).hide();
                        $(".payment-uniq", page.el).attr("disabled", true).show();
        
                        $('.label-orderfee', page.el).html(numeral(0).format('0,0'));
                        $('[name=Admin_Fee]', page.el).val(0);
                        $('.label-ordertotal', page.el).html(numeral(parseFloat(amt) + parseFloat(uniq) - parseFloat(memberPointAmount)).format('0,0'));
                    }
                });
                $("[name=Bank_Id]:checked", page.el).trigger("change");
            }, function() {
        
            }, function() {
        
            });
        }
        else {
            app.dialog.alert(dat.Message);
        }

    }, function() {

    }, function() {

    });

    $('.btn-submit', page.el).click(function() {
        var bank_id = $('[name=Bank_Id]:checked', page.el).val();
        var pin = $('[name=PIN]', page.el).val();
        var amount = $('[name=Amount]', page.el).val();
        var uniq = $('[name=Unique_Amount]', page.el).val();
        var fee = $('[name=Admin_Fee]', page.el).val();
        var memberPointAmount = $('[name=Member_Point_Amount]', page.el).val();

        if(fee && fee > 0) {
            amount = parseFloat(amount) - parseFloat(memberPointAmount) + parseFloat(fee);
        } else {
            amount = parseFloat(amount) + parseFloat(uniq) -  parseFloat(memberPointAmount);
        }

        if(!bank_id || !pin || amount < 0) {
            var mes = '';
            if(!bank_id) mes = 'Silahkan pilih rekening bank.';
            if(!pin) mes = 'Silahkan isi PIN.';
            if(amount < 0) mes = 'Nilai transaksi tidak valid.';

            var toastError = app.toast.create({
                text: mes,
                icon: '<i class="f7-icons">close_round</i>',
                position: 'center',
                closeTimeout: 2000
            });
            toastError.open();
        }
        else {
            var model = {
                Order_id: orderId,
                Bank_Id: bank_id,
                Security_PIN: pin,
                Amount: amount,
                Member_Point_Amount: memberPointAmount
            };

            app.preloader.show();
            getData('CreatePayment', {model: model, UUID: auth.UUID}, function(data) {
                var dat = data.res;
                if(dat.Status == COMMON_OK) {
                    var orderId = dat.Data.Id;
                    app.views.current.router.navigate('/finish/'+model.Order_id+'/1/');
                }
                else {
                    app.dialog.alert(dat.Message);
                }
            }, function() {
                app.dialog.alert("Something gone wrong..");
            }, function() {
                app.preloader.hide();
            });
        }
    });
});

$$(document).on('page:init', '.page[data-name="finish"]', function (e, page) {
    var auth = getLocalStorage('auth');
    var paymentId = page.route.params.paymentId;
    var mode = page.route.params.mode;
    if(mode == -1) {
        $(".navbar-inner", page.el).prepend('<div class="left"><a class="link back"><i class="icon icon-back"></i> </a> </div>');
    }
    else {
        homeView.router.clearPreviousHistory();
    }

    app.preloader.show();
    getData('GetPaymentDetail', {id: parseInt(paymentId), UUID: auth.UUID}, function(data) {
        var dat = data.res;
        if(dat.Status == COMMON_OK) {
            var payment = dat.Data;

            $('.list', page.el).removeClass('skeleton-text skeleton-effect-fade');
            $('.label-paymentno', page.el).html(payment.Payment_Number);
            $('.label-orderno', page.el).html(payment.Order_Number);
            $('.label-orderdate', page.el).html(moment(payment.Order_Date).format('DD/MM/YYYY'));
            $('.label-ordername', page.el).html(payment.Order_Name);
            $('.label-orderphone', page.el).html(payment.Order_Phone?payment.Order_Phone:'-');
            $('.label-ordermail', page.el).html(payment.Order_Email);
            $('.img-bankicon', page.el).html('<img src="'+payment.Bank_Icon+'" height="24px" width="56px" />');
            $('.label-bankname', page.el).html(payment.Bank_Name);
            $('.label-bankaccount', page.el).html('<span style="font-weight: bold">'+payment.Bank_Account_No+'</span>'+ (payment.Bank_Account_Name ? ' A/N. '+'<span style="font-weight: bold">'+payment.Bank_Account_Name+'</span>' : ""));
            $('.label-paymenttotal', page.el).html(numeral(payment.Amount).format('0,0'));
            $('.label-paymentlimit', page.el).html(moment(payment.Limit_Time).format('DD/MM/YYYY HH:mm'));

            var icon = '';
            var status = payment.Status;
            if (status == 'Verified') {
                $('.row-reveal', page.el).show();
                $('.row-confirmation', page.el).hide();
                icon = '<i class="icon f7-icons text-color-green" style="font-size: 11pt; font-weight: bold; line-height: 1.5">check_round</i>';
            }
            else if (status == 'Not Verified') {
                $('.row-reveal', page.el).hide();
                $('.row-confirmation', page.el).show();
                icon = '<i class="icon f7-icons" style="font-size: 11pt; font-weight: bold; line-height: 1.5">stopwatch</i>';
            }
            else if (status == 'Expired') {
                $('.row-reveal', page.el).hide();
                $('.row-confirmation', page.el).hide();
                icon = '<i class="icon f7-icons text-color-red" style="font-size: 11pt; font-weight: bold; line-height: 1.5">stopwatch</i>';
            }
            else if (status == 'Waiting') {
                $('.row-reveal', page.el).hide();
                $('.row-confirmation', page.el).show();
                icon = '<i class="icon f7-icons" style="font-size: 11pt; font-weight: bold; line-height: 1.5">help_round</i>';
            }
            $('.label-paymentstatus', page.el).html(icon+'&nbsp;'+status);

            $('.payment-items', page.el).empty();
            var html = '';
            for(var i=0; i<payment.Items.length; i++) {
                html += '<li class="item-content">';
                html += '<div class="item-media">';
                html += '<img src="'+payment.Items[i].Icon_Image_File+'" style="width: 36px; height: 36px" />';
                html += '</div>';
                html += '<div class="item-inner">';
                html += '<div class="item-title-row">';
                html += '<div class="item-title">'+payment.Items[i].Voucher_Name+'</div>';
                html += '</div>';
                html += '<div class="item-subtitle">'+payment.Items[i].Description+' x '+numeral(payment.Items[i].Quantity).format('0,0')+'</div>';
                html += '</div>';
                html += '</li>';
            }
            $('.payment-items', page.el).html(html);

            $('.btn-upload', page.el).click(function() {
                navigator.camera.getPicture(
                    function(imageURI){
                        app.dialog.alert("Galery cb");
                        var options = new FileUploadOptions();
                        options.fileKey = "ConfirmationFile";
                        options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
                        options.mimeType = "image/jpeg";
                        options.params = {Id: paymentId};
                        var ft = new FileTransfer();
                        ft.upload(imageURI, INDOGAMECARDURL+"UploadPaymentConfirm",
                            function (result) {
                                app.dialog.alert("Upload cb");
                                var toast = app.toast.create({
                                    text: /*'Bukti Pembayaran terkirim.'*/JSON.stringify(result),
                                    icon: '<i class="f7-icons">check</i>',
                                    position: 'center',
                                    closeTimeout: 2000,
                                    on: {
                                        closed: function () {
                                            app.views.current.router.navigate('/');
                                        }
                                    }
                                });
                                toast.open();
                            },
                            function (error) {
                                console.log(error);
                                app.dialog.alert(result.Message);
                            }, options);
                    },
                    function(err){
                        console.log(err);
                        app.dialog.alert("Gagal membuka file system.");
                        return false;
                    },
                    {
                        quality: 100,
                        destinationType: navigator.camera.DestinationType.FILE_URI,
                        sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
                    }
                );
            });

            var param = {PIN: auth.PIN, UserId: auth.User_Id, OrderID: payment.Order_Id};
            console.log(param);
            $('.btn-reveal', page.el).click(function() {
                app.preloader.show();
                getData('GetVoucherCode', param, function(data) {
                    var dat = data.res;
                    if(dat.Status == COMMON_OK) {
                        var html = '';
                        html += '<div class="popup"><div class="block">';
                        for(var i=0; i<dat.Data.length; i++) {
                            html += '<table>'+
                                    '<tr><td style="white-space: nowrap; vertical-align: top">Produk</td><td style="vertical-align: top">:</td><td>'+dat.Data[i].Item+'</td></tr>'+
                                    '<tr><td style="white-space: nowrap;">No. Serial</td><td>:</td><td>'+dat.Data[i].Serial_Number+'</td></tr>'+
                                    '<tr><td style="white-space: nowrap;">Code</td><td>:</td><td>'+dat.Data[i].Code+'</td></tr>'+
                                    '<tr><td style="white-space: nowrap;">Password</td><td>:</td><td>'+dat.Data[i].Password+'</td></tr>'+
                                    '</table>'+
                                    '<hr />';
                        }
                        html += '<p><a href="#" class="link popup-close">Tutup</a></p>';
                        html += '</div></div>';

                        console.log('html', html);
                        var revealPopup = app.popup.create({content: html});
                        revealPopup.open();
                    }
                    else {
                        app.dialog.alert(result.Message);
                    }
                }, function() {
                    app.dialog.alert("Something gone wrong..");
                }, function() {
                    app.preloader.hide();
                });
            });
        }
        else {
            app.dialog.alert(dat.Message);
            app.views.current.router.navigate('/');
        }

    }, function() {
        app.dialog.alert("Something gone wrong..");
    }, function() {
        app.preloader.hide();
    });
});

$$(document).on('page:init', '.page[data-name="history"]', function (e, page) {    
    var auth = getLocalStorage('auth');
    getData('GetUserPayments', {UUID: auth.UUID}, function(data) {
        var dat = data.res;
        if(dat.Status == COMMON_OK) {
            setLocalStorage('history', dat.Data);
            drawOrderHistory($('.history', page.el));
        }
        else {

        }
    }, function() {

    }, function() {
    });
});

$$(document).on('page:init', '.page[data-name="historypoint"]', function (e, page) {
    drawPointHistory($('.historypoint', page.el));
});

